# ICR Predictor with MLOps pipeline

## Descriprion
Project for train icr classification model (https://www.kaggle.com/competitions/icr-identify-age-related-conditions)
and running trained model as API service.


## Installation
Install icr component as python package:  
`cd ./components/icr_model`  
`pip install -e .[dev]`

For training: use `./components/icr_model/composites/train_model.py`.  
For using predictor API run `./entrypoint_api.sh` OR build 
`./deployment/Dockerfile` and start it.

For dvc s3-storage connection: add secret_access_key to `.dvc/config`.
Pull train dataset: `dvc pull`
