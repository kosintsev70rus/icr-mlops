from typing import Dict, Union
from flask import Flask, request
from application.predictor.service import Predictor
from application.predictor.dto import RowFeatures
from dacite import from_dict


def create_app(predictor: Predictor):
    app = Flask(__name__)

    @app.route('/predict')
    def get_prediction():
        rows_features = json_to_idx_row_features(request.json)

        predictor.get_prediction(rows_features=rows_features)

    @app.route('/model_features')
    def get_features():
        return RowFeatures.__dict__['__annotations__']

    def json_to_idx_row_features(
            json_data: Dict[str, Dict[str, Union[float, str]]]
    ) -> Dict[str, RowFeatures]:
        idx_row_features = {}
        for idx in json_data.keys():
            idx_row_features = {
                idx: from_dict(data_class=RowFeatures, data=json_data)
            }

        return idx_row_features

    return app
