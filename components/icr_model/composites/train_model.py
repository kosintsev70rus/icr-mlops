from pathlib import Path
import mlflow
import numpy as np

from application.predictor.service import FeatureEngineering, ModelTrainer
from .Settings import TrainSettings


class Train:
    def __init__(
            self,
            feature_engineering: FeatureEngineering,
            train_data_path: Path,
            target_name: str
    ):
        self.feature_engineering = feature_engineering
        self.train_data_path = train_data_path
        self.target_name = target_name

    def train_eval_models(self):
        model_trainer = ModelTrainer(self.feature_engineering)
        model_trainer.train_eval_model(self.train_data_path, self.target_name)


if __name__ == '__main__':
    train_settings = TrainSettings()
    np.random.seed(train_settings.RANDOM_SEED)

    mlflow.set_tracking_uri(train_settings.MLFLOW_URL)

    Train(
        feature_engineering=FeatureEngineering(),
        train_data_path=train_settings.TRAIN_DATA_PATH,
        target_name=train_settings.TARGET_NAME,
    )
