from adapters.prediction_api.app import create_app
from application.predictor.service import Predictor, FeatureEngineering
from .Settings import InferenceSettings

inference_settings = InferenceSettings()

app = create_app(
    Predictor(
        feature_engineering=FeatureEngineering(),
        mlflow_id=inference_settings.MLFLOW_MODEL_ID,
    )
)
