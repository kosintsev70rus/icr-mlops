from pydantic import BaseSettings
from pathlib import Path


class TrainSettings(BaseSettings):
    TRAIN_DATA_PATH: Path
    MLFLOW_URL: str
    TARGET_NAME: str = 'Class'
    RANDOM_SEED: int = 42


class InferenceSettings(BaseSettings):
    MLFLOW_MODEL_ID: str
