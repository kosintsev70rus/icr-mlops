import datetime
from typing import Dict
from pathlib import Path
from dataclasses import asdict

import mlflow
import pickle
import pandas as pd
from lightgbm import LGBMClassifier
from sklearn.metrics import roc_auc_score, f1_score
from sklearn.model_selection import train_test_split

from .dto import Predictions, RowFeatures


class FeatureEngineering:

    @staticmethod
    def prepare_historical_data(features: pd.DataFrame) -> pd.DataFrame:
        return features.fillna(0)

    @staticmethod
    def prepare_actual_data(features: Dict[str, RowFeatures]) -> pd.DataFrame:
        df_dict = {}
        for idx in features.keys():
            df_dict[idx] = asdict(RowFeatures)

        return pd.DataFrame.from_dict(df_dict)


class Predictor:
    def __init__(self, feature_engineering: FeatureEngineering, mlflow_id: str):
        self.feature_engineering = feature_engineering
        self.ml_model = self._load_model(mlflow_id)

    @staticmethod
    def _load_model(mlflow_id: str) -> mlflow.pyfunc.PyFuncModel:
        return mlflow.pyfunc.load_model(mlflow_id)

    def get_prediction(
        self, rows_features: Dict[str, RowFeatures]
    ) -> Predictions:
        prepared_data = self.feature_engineering.prepare_actual_data(
            rows_features
        )

        model_predictions = self.ml_model.predict(prepared_data)
        predictions = Predictions()
        predictions.values = {
            id: val
            for id, val in zip(rows_features.keys(), model_predictions)
        }

        return predictions


class ModelTrainer:
    def __init__(self, feature_engineering: FeatureEngineering):
        self.feature_engineering = feature_engineering

    def train_eval_model(self, train_data_path: Path, target_name: str):
        train_df = self._load_local_data(train_data_path)
        prepared_data = self.feature_engineering.prepare_historical_data(
            train_df
        )
        # simple baseline for classifier
        classifier = LGBMClassifier()

        x_test, y_test, x_train, y_train = train_test_split(
            prepared_data.drop(target_name), prepared_data[target_name], 0.2
        )

        classifier.fit(x_train, y_train)
        experiment_start = datetime.datetime.now()
        model_path = Path(f'./temp/{experiment_start}/classifier.pkl')
        with open(model_path) as file:
            pickle.dump(classifier, file)

        predicts = classifier.predict(x_test)
        metrics = self._evaluate_model(predicts, y_test)

        self._log_mlflow(metrics, model_path, experiment_start)

    @staticmethod
    def _log_mlflow(
            metrics: Dict[str, float],
            model_path: Path,
            experiment_start: datetime.datetime,
    ):

        mlflow.create_experiment(str(experiment_start))

        mlflow.log_metric('f1_score', metrics['f1_score'])
        mlflow.log_metric('roc_auc', metrics['roc_auc_score'])
        mlflow.log_artifacts(str(model_path))
        mlflow.log_param("timestamp", pd.Timestamp.now())

    @staticmethod
    def _evaluate_model(
            fact: pd.Series, predict: pd.Series
    ) -> Dict[str, float]:
        metrics = {
            'f1_score': f1_score(fact, predict),
            'roc_auc_score': roc_auc_score(fact, predict),
        }
        return metrics

    @staticmethod
    def _load_local_data(local_data_path: Path) -> pd.DataFrame:
        file_ext = local_data_path.name.split('.')[-1]
        if file_ext == 'csv':
            df = pd.read_csv(local_data_path)
        elif file_ext in ['xls', 'xlsx']:
            df = pd.read_excel(local_data_path)
        else:
            raise ValueError(f'I do not know this file extension: {file_ext}')

        return df
