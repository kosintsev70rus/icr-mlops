from typing import Dict

from dataclasses import dataclass


class Predictions(dataclass):
    # Key is id, value is predicted value
    values: Dict[str, float]


class RowFeatures(dataclass):
    AB: float
    AF: float
    AH: float
    AX: float
    AY: float
    AZ: float
    BC: float
    BD: float
    BN: float
    BP: float
    BQ: float
    BR: float
    BZ: float
    CB: float
    CC: float
    CD: float
    CF: float
    CH: float
    CL: float
    CR: float
    CS: float
    CU: float
    CW: float
    DA: float
    DE: float
    DF: float
    DH: float
    DI: float
    DL: float
    DN: float
    DU: float
    DV: float
    DY: float
    EB: float
    EE: float
    EG: float
    EH: float
    EJ: str
    EL: float
    EP: float
    EU: float
    FC: float
    FD: float
    FE: float
    FI: float
    FL: float
    FR: float
    FS: float
    GB: float
    GE: float
    GF: float
    GH: float
    GI: float
    GL: float
